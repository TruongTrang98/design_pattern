package strategy_pattern;

public interface IQuackBehavior {
	public void quack();
}
