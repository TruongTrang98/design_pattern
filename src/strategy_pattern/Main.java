package strategy_pattern;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MallardDuck mallarDuck = new MallardDuck();
		mallarDuck.display();
		mallarDuck.setFlyBehavior(new FlyNoWay());
		mallarDuck.performFly();
		mallarDuck.performQuack();
		
		System.out.println("========================");
		
		ModelDuck modelDuck = new ModelDuck();
		modelDuck.performFly();
		
		modelDuck.setFlyBehavior(new FlyRocketPowered());
		modelDuck.setQuackBehavior(new Quack());
		
		modelDuck.display();
		modelDuck.performFly();
		modelDuck.performQuack();
	}

}
