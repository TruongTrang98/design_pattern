package strategy_pattern;

public class MallardDuck extends Duck{
	public MallardDuck() {
		// TODO Auto-generated constructor stub
		flyBehavior = new FlyNoWay();
		quackBehavior = new Squeak();
	}
	
	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("MallarDuck");
	}
	
	
}
