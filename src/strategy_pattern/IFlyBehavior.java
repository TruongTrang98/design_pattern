package strategy_pattern;

public interface IFlyBehavior {
	public void fly();
}
